#01. MDEF BOOTCAMP


#Wednesday 5 October 2018

#PREFACE:
Well here we are, the web site is up and running. I'm not sure about the complexity involved in creating this site however, which was incomprehensible for all of us and certainly we could not have done it on our own - even the instructor had problems making it all work. I'm also feeling unsure of the complicated system to organise our documents. The system was developed for programers and in a situation where people are working with abstract symbols on code this approach probably works but designing physical manifestations is something quite different. The story about the elephant is a horse designed by a committee comes to mind, where a multitude of ideas have no over all cohesion that is only possible with one mind. The end result may have been created cooperation but the result can only be a conglomeration of ideas so this is of concern to me. Coding is a very different process than designing physical manifestations that have form and I think we need to be careful we don't lose the plot here. I remain to be convinced that this elaborate system will actually help us and that perhaps siting down together as a team and simply talking might be much more productive.

- So I'm willing to stay with this process and see what happens and see if I can learn something new from trying out this system. I haven't had a chance to use the system and no one I know in our class has figured out how to use it yet.

- I can't yet see that it will be relevant to our work and it seems that there are other systems that are much easier to use that I know other professionals are using - but as I said I'll give it a try with an open mind.   

#Thursday 6 October 2018

#PREFACE:
Today we visited various local businesses that are on the city blocks close to IAAC. Virtually all the businesses were involved in producing something physical. The first location we visited was Leka restaurant that had been designed and produced at IAAC. The chairs, tables and virtually the entire interior decor had been made from plywood in the IAAC FabLab. All the designs were open source so each piece of furniture had a barcode that that it could be scanned with a phone and leads to the files needed to reproduce the product - or the entire restaurant it you want. The second visit was called Indissoluble, an exhibition and events management company, which was I believe developed by Architects. We saw their design studios and workshop which appeared to use a lot of extrusions as the basis of their designs but they also had a sheet metal bender and a CNC machine for cutting wood and light metals. To me they appeared to be a standard exhibition fabrication company. Their offices were the high light for me with an interesting good example of the big table concept being used correctly by separating the tables into four different clusters and there were another two big tables in glass meeting rooms.We visited a workshop called Transfolab that ran workshops to reused various found materials and make them into new products. Another visit was a bike shop called Biciclot that used bicycles strategically to as means to engage and teach people the skills of building and repairing bikes and their goal is to renovate an adjoining building and create a social hub around the concept of bicycles. We also stopped in the middle of an intersection, which had been pedestrianised and talked about the Super Block problems.

![](Brain.jpg)

Photo: Jonathan Crinion
Description: Indissoluble exhibition project about the Brain.

![](Bike.jpg)

Photo: Jonathan Crinion
Description: Biciclot - Tools for fixing bikes.

- I had been exploring the areas previously and so it was interesting for me to learn that various businesses are open at different times of the day. There appears to be a rhythm as to when businesses operate that I have yet to discover but I am sure with time will become apparent.

- My learning point is that the Super Block concept appears to have been compromised by a bad implementation process and lack of consultation with the locals.

- For me most of the trip visits were interesting but I didn't really see anything I haven't seen or heard about before. The Super Block concept interests me and appears to have so much potential and I'd like to learn more.

- The trip was useful to get a sense of how people are socially oriented. I can see that there are businesses that are typical metal workshops, wood workers, materials recycling etc. and that there are numerous social enterprises using the making process to engage with the public.  

#Friday 7 October 2018

#PREFACE:
The day began with Tomas talking about FabLabs. Saira asked if cities need to be located in ‘sweet spots’ close to the resources that they will need to manage in order to produce the things they wan to produce. Adam Curtis’ movie The Century of Self came up as a must see movie, which is about how a person named Edward Bernays used Freud’s theories to create what today has is called advertising as a means to persuade people to buy products. Later Oscar  Tomico spoke about wearable technological clothing. Later we discussed the materials we had found the day bore that we hope to utilise at a later date and after that we did a very fast exercise in our groups to see what we had been learning.

- I learned that the FabLab concept has been largely focused on the technological side of making and networking but materials such as oil based plastics are still in use for 3D printing and that things like plywood are still being shipped great distances.

- While the FabLab concept is impressive as to it’s extent it is basically at present an affirmative design project in that it has not yet resolved the material side of the problem. I would speculate that because of this and also because it is a distributed system it is presently causing an increase in its carbon footprint due to the Inefficiency of materials handling and shipping compared to centralised systems. I’m learning that there is big opportunity to resolve the issue of how to for example smelt metals in the city or grow the trees needed for fabrication on a scale which is viable interns of keeping up with production.

- Adding technology to materials created a big question for me, especially the terms like ‘ultrapersonalization as it was used to make shoes (that looked very much like it was imitating Ross Lovegrove’s shoe design for United Nude  http://www.arturotedeschi.com/wordpress/?page_id=6981). I could not but feel that this was all about increasing sales. I liked the luminous clothing for runners, which had a practical application and I was impressed by how technology could be made flexible - I wondered how it might survive the washing machine?

- We reviewed on a map the Poblenou area and discussed various bignesses an speculate on how some thing new could be created by connecting them with other businesses. The prices is reminiscent of the Memphis movement in Italy where designers engaged with existing businesses and then proposed that they could use their equipment to make alternative products - for example a grave stone maker uses his skills to make various furniture components such a stamp bases

![](Mapping.jpg)

Photo: Jonathan Crinion
Description: Mapping Poblenou
