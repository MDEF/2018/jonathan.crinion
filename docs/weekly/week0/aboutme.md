### About me

We were asked three questions: 

- 'Who are you' 
 
- 'What do you want to get out of this course'

- 'In ten years where do you want to be'
 
In answer to the first question I used to write the following treatise below, but that is all about my past past, which for the most part was about 'affirmative design' and does not relate much to where I want to go now. So I'd like to say something new, which is: I am a 'creative' with a visual acuity that senses new possibilities. I'm also an ecological scientist and a human geographer and I want to combine those skills with my visual sensibility to create situations that enable people to make a difference in the world. By answering this question differently than the standard form below, which is all about listing achievements I can sense that I have new possibilities. 

In answer to the second question about what do I want to get out of this course, I would answer, 'a different way of thinking and perceiving the world and how to practically take that experience back into the world'. The final question about where I want to be in ten years is that I would like to create a creative studio again in a very different manner than my previous consulting practice. I am inspired by the famous graphic designer Otl Aicher's studio at Ulm Germany in the country side where he and his employees lived and worked. To this set up I would add a Social Co-operative working model where I am not the main character but rather I am part of a peer research group.  
 
The outdated me:

Jonathan Crinion was born in Liverpool, England in 1953 and moved to Canada before returning to the UK. His father was an Architect and his mother a Fashion Designer, which greatly influenced his design perspectives.

He began working on environmental issues in the early 1970's and graduated as an Associate to the Ontario College of Art and Design (AOCA) in the Department of Product and Systems Design with a thesis project of solar energy. After working with Kuypers Adamson & Norton (KAN) and Foster Associates Architects London he formed Crinion Associates in 1986, which became an international Research, Planning and Design consulting firm. He holds an MSc with Distinction in Holistic Science (Earth Systems Science) at Schumacher College, Devon, UK where he explored Holistic Design Ecology. Additionally Crinion holds a Doctorate of Philosophy in Human Geography from Exeter University, UK. His Doctoral thesis was about how eco-psychology could inform social change in relation to human caused environmental degradation of which climate change is but one part.

Crinion Associates grew to international fame and won many prestigious awards for outstanding design excellence. The company engaged in projects ranging from vertical axis wind turbines for domestic use, electronic equipment, to the ‘Open Table’ concept for Knoll International, NY, which has revolutionised the office work environment globally. 

In 1994 Jonathan Crinion was chosen as one of the top 40 designers in the world by International Design Magazine. In 1995 Crinion Associates received two prestigious Financial Post Design Effectiveness Awards, one for Best of Product Development and one for Best of Show for the companies work with Knoll International in New York. Crinion is the Winner of the 1999 FX Design awards UK, Winner of the 1999 International Design Review - Best of Category and the Winner of the 2000 Toronto Arts Award for Design and Architecture. In 2002 he won the international D&AD Award for Outstanding Product Design in the UK. In 2004 he was the winner of the IIDEX International Show - Bronze, Silver and Gold awards for his designs and innovations in the work environment. 

He is an occasional visiting lecturer at Schumacher College, The International Centre for Ecological Studies and recently has been a keynote speaker at the Victoria & Albert Museum London, The Eco Design Centre Wales, The Reform Conference in Ireland, The Institute of Technology in Carlow and at the DEEDS conference on Ecological Education at Brighton University, among other universities and public venues. 

In 1997 the Queen's representative, the Governor General of Canada, Roméo Le Blanc presented Jonathan Crinion with the prestigious Royal Canadian Academy of the Arts (RCA) designation in recognition of his design achievements. He is a past member of the Chartered Society of Designers in the UK and past Director of Communications for the Association of Canadian Industrial Designers. 

Crinion is also one of the three founding member/owners and is a Creative Advisor of Gaia y Sofia, a Social Cooperative Company based in Asturias, Spain. Gaia y Sofia offers transformative events and courses for individuals, organizations, institutions and communities. 

Prior to 2004 Crinion was the President and principal shareholder of Crinion Associates Ltd an international Research, Planning and Design firm established in 1986, with a head office in Toronto, Canada. Crinion retired from the company in 2004 and is now  an independent consultant, specialising in Ecological Design Consultant in London, UK and  remains a Consultant to consultant to Crinion Associates, which continues to hold and manage numerous international design patents. 

In the past, working in the capacity of Design Director and managing a team of over 20 designers at Crinion Associates Ltd, Crinion oversaw multi million pound product and systems development projects for many private and international corporations. In his capacity as a retired consultant, Jonathan Crinion now designs and patents products and systems and then licenses them to appropriate manufactures. Rather than offering design services directly to customers as Crinion Associates did, Jonathan Crinion now develops ecological products and systems for licensing.

In 2006 Crinion created a sailing project in collaboration with Owen Clark Naval Architects in Devon, UK  for 'Friends of the Earth' in London, UK to promote carbon reduction for a project called The Big Ask. He sailed  the Friends of the Earth yacht with friend Brian McCallum from Cape Town, South Africa to Madeira and then single handed from Madeira to Falmouth, UK. 


Central to Crinion’s success is his belief that ‘less is more’ which is achieved through disciplined innovation involving a common theme of innovation and material reduction. His resolved forms reflect frugality, often combining many functions into one simple part. For Crinion, environmental consciousness is nothing new, having begun in earnest when he designed a solar hot water heating system in the 70's. After many years Jonathan’s designs are showing that they have a sense of quiet timelessness. This comes from the complex but restrained ideas he generates. He refrains from participation in the rabid consumerism that has gripped industry. His philosophy lies in complete opposition to needless proliferation of 'Designer products' with short life cycles, believing that 'the future of design lies in finding local or non material solutions'. His latest work begins to resolve the disparity between the corporate fight for limited global resources by engaging local networks of skilled services and sustainable material providers and by using clever Peer Production Licensing techniques to promote decentralised ecological bioregional production over centralised international sourcing.


His interests are in the metaphysics of the Human Geography of ‘Creativity – Made Visible’ and how a new relationship with human creativity may be translated into an ecological polity. To that end he is pursuing work on ‘Situational Expeditions’, which are designed to help existential individuals catalyze specific combinations of efficacy and agency into situations that allows them with foresight, to create social action for an ecologically connected future.

He is an experienced long distance single-handed offshore sailor, a skill he uses as a means of travel. He lives in London England and works globally.

