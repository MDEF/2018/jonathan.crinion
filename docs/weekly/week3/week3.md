#W3 DESIGN FOR THE REAL DIGITAL WORLD

#Monday 15 October 2018


- We begin with an explantation about a project for the week. The objective is to learn about 3D printing, Laser cutting, CNC cutting and using a sewing machine. We watched an informative movie by Space 10 called 'The Made Again Challenge'.

- I could feel I was getting (again) frustrated at IAACs's approach, which is to engage with symptoms rather than root causes. So I was developing a concern that this project, by using waste materials rather than questioning why they are in the garbage and how to prevent them in the first place - We were instead dealing with the symptoms of the problem. By using waste materials we are only down-cycling (further degrading the materials) the materials and at the same time giving the makers of the things that are being disposed of the ability to think they don't need to worry about creating bad products. In essence, I argue that this approach is in fact exacerbating the problem by giving license to the producers to think what they are doing is ok. I propose instead that tackle the real political and economic problem of why junk products exist in the first place.

- It occurs to me that the reason we are doing it this way is to experience how much material is wasted (Problem 1) and that we use these materials so we can learn to to use the various machines (Problem 2). However these are unrelated problems. Perhaps a project where we go and engage with the manufacturers of the products we find and try and work with them to use their skills to create more useful products that don't end up in the garbage could be more useful, last longer and be easily recycled would be a better project and that learning to use the digital equipment is then attached to that project.    

- The proposed task is that we redesign our home room 201 and make it more suitable for the things we will be doing and in the process that we will design some elements that will become part of the room. We were divided into 4 groups of 7 to develop our designs.

![](Tables.jpg)
We tried many layouts and realise we can use the exiting tables. The tin foil is left over from a huge chocolate bar we ate in about 5 seconds Photo: Jonathan Crinion

![](Spaceplan.jpg)
We cut out table shapes to scale to try alternative layouts. Photo: Jonathan Crinion

- We spent a lot of time in our group analysing how we could place the tables in the back part of the room , which is larger than the front in such a way that no people had the posts in their view. We developed may scenarios and in the end we discovered that there are already tables in the room that can work very well if we eliminate a lot of the things in the room that we are not using. At the end of the day we only need 8 tables for the students and one for the lecturer and that we can eliminate all the other tables.

- Our group then focused on the front half of the room, which we had decided was more of a lounge area with low seating with a screen for creating a division for the lecture area at the back and a coffee bar in the front area where we could recycle the coffee grounds for growing mushrooms.

- We concluded the day by deciding to develop the coffee bar and the screen for the presentation tomorrow.


![](Coffee1.jpg)
Preliminary coffee and mushroom table concept. Photo Jonathan Crinion

![](Coffee2.jpg)
Refined coffee and mushroom table concept. Photo: Jonathan Crinion


- We roam the streets late into the night looking for the parts to make our concepts. I find some flower pots that we need. There is also a screen concept that we need parts for too but I didn't take a photo.    
